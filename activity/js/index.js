// console log the message Hello World
console.log("Hello World")

let user = {
	firstName: "", 
	lastName: "",
	age: 0,
	hobbies: ["Biking", "Mountain", "Climbing", "Swimming"],
	workAddress: {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	}
}

// Check if the item is of object type
const isObject = function(val) {
	return  ( (val !== null) && (typeof val === 'object') ) ? true : false
}

// Create a function named printUserInfo that will accept the following information:
// - First name
// - Last name
// - Age
function printUserInfo(fn, ln, age) {
	user.firstName = fn
	user.lastName = ln
	user.age = age
	let item = ""

	//The printUserInfo function will print those details in the console as a single string
	console.log(user.firstName, user.lastName, user.age)
	
	//This function will also print the hobbies and work address.
	for (let val in user) {
		if (isObject(user[val])) {	

			for (let val2 in user[val]) {				
				item += user[val][val2] + " "
			}

			console.log(item)
			item = ""	
		} else {
			//do nothing
		}				
		
	}
	console.log(item)
}

// envoke function
printUserInfo("John", "Smith", 30)