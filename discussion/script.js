//Writing Comments
	//Comments are meant to describe the written code.

//Two ways of writing comments in JS:
	//1. Single line
		//ctrl + /

	/*
	 2. multi line comment
		(ctrl + shift + /)
	*/


//Syntax and Statements
	/*
		Syntax - set of rules that describes how statements must be constructed.

		Statements - set of instructions, and ends with a semicolon.

	*/
//alert("Hi!")
console.log("Hello World"); //logs this message on the console

	/*
		Syntax: 
			console.log(message)
		Statement:
			console.log("Hello World")

	*/

//Storing Values
	
	//Variables
		//-container that holds a value
		//value - a data that you assign to a variable

//Declaring Variables
	//Syntax:
		//let/const variableName;

	let myVariable;
	console.log(myVariable);

	//Variables must be declared first before they are used
		//console.log(hello);
		//let hello;

//Guidelines in Writing in Variables
	/*
		1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator(=) to assign a value.
		2. Variable names should start with a lowercase character, use camelCase for multiple words.
		3. For constant variables, use the 'const' keyword.
		4. Variable names should be descriptive of the value being stored to avoid confusion.

		let myBatch = 157
	*/

//Declaring and initializing variables
	//Syntax:
		//let/const variableName = value;

	let productName = 'desktop computer';
	console.log(productName)

	let productPrice = 18999;
	console.log(productPrice);

	const pi = 3.1416;
	console.log(pi);

//Reassigning a variable values
	//Syntax:
		//variableName = newValue

	productName = 'Laptop';
	console.log(productName);

	//Values of constants cannot be changed and will simply return an error
	//pi = 3.1234;
	//console.log(pi); //Uncaught TypeError: Assignment to constant variable.

//Reassigning variable vs. initializing variables
	//Declares a variable first
	let supplier;
	//This is considered as initialization because it is the first time that a value has been assigned to a variable
	supplier = "John Smith Tradings";
	console.log(supplier);

	//This is considered as reassignment because its initial value was already declared.
	supplier = "Zuitt Store";
	console.log(supplier);

//Multiple Variable Declaration
	let productCode = 'DC011', productBrand = 'Dell';
	console.log(productCode, productBrand);

//Using a variable with a reserved keyword
	//const let = "Hello";
	//console.log(let)


//DATA TYPES
	//1. Strings
		//-series of characters that create a word, phrase, sentence or anything related to creating a text
		//- alphanumeric
		//Strings in JS can be written using a single quote (''), double quote("") or back ticks(``).


	let city = 'Pasig City';
	let province = "Metro Manila";
	let country = `Philippines`;

	console.log(city);
	console.log(province);
	console.log(country);

	console.log(city, province);

//Concatenate Strings
	let fullAddress = province + ','+ country;
	console.log(fullAddress);

	 let greeting = 'I live in the ' + country;
	 console.log(greeting);

	 //"/n" refers to creating a new line in between text
	 let mailAddress = 'Metro Manila \n\n Philippines';
	 console.log(mailAddress);

	 let message = "John's employees went home early!";
	 console.log(message);

	 message = 'John\'s employees went home late!';
	 console.log(message);


	 //2. Numbers
	 	let headCount = 26;
	 	console.log(headCount);

	 	let grade = 98.7;
	 	console.log(grade);

	 	let planetDistance = 2e10;
	 	console.log(planetDistance);

	//Combining text and numbers
		console.log("John's grade last quarter is " + grade);


	//3. Boolean
		//true or false

		let isMarried = true;
		let inGoodConduct = false;

		console.log("isMarried:" + isMarried);
		console.log("inGoodConduct" + inGoodConduct);

	//4. Arrays
		// Arrays are special kind of data type that's used to store multiple values

		//Similar Data Types:
			//Syntax:
				//let/const arrayName = [elementA, elementB ...]
			let grades = [98.7, 92.1, 90.2, 94.6];
			console.log(grades);

		//Different Data Types:
			let details = ["John","Smith", 32, true];
			console.log(details);

	//5. Objects
		// Objects is used to create a complex data that contains a pieces of information that are relevant to each other.

		//Syntax:
			/*
				let/const objectName = {
							propertyA: value,
							propertyB: value,
							...
				}
			*/

			let person = {
				fullName: "Juan Dela Cruz",
				age: 25,
				isMarried: false,
				address: {
					houseNumber: "345",
					city: "Manila"
				}
			}

			console.log(person);

	