let favFood = "Jollibee Fried Chicken"
const sum = (a , b) => console.log(a + "+" + b + " = " + (a + b))
const product = (a, b) => console.log(a + "x" + b + " = " + (a * b))
let isUserActive = true
let favResto = ["Jollibee", "McDo", "KFC", "Mang Inasal"]
let favArtist = {
	firstName	: "Avril",
	lastName	: "Lavigne" ,
	stageName	: "Avril Lavigne",
	birthday	: "September 27, 1984",
	age 		: 38,
	bestAlbum	: "Let Go",
	bestSong	: "I'm with You",
	isActive 	: true
}
const quotient = (a, b) => {
	switch (true) {
		case (a === 0 && b === 0):
			console.log(a + "/" + b + " = Result is undefined")
			break
		case b === 0:
			console.log(a + "/" + b + " = Cannot divide by zero")
			break
		default:
			console.log(a + "/" + b + " = " + a / b)
	}
}

console.log("Favorite Food: " + favFood)
sum(1,1)
product(3,6)
console.log("isUserActive: " + isUserActive)
console.log("Favorite Restaurant: " + favResto.join(', '))
console.log("Favorite Artist:")

const isObject = val => (val !== null && (typeof val === 'object')) ? true : false
const objProps = obj => {
	for (let prop in obj) {
		if (isObject(obj[prop])) {
			objProps(obj[prop])
		} else {
			console.log(prop + "\t:\t", obj[prop])
		}
	}
}
objProps(favArtist)
quotient(2,2)